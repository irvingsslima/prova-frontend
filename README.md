![Context LMS](assets/logo.png)

# Desafio Front-End

O desafio consiste em criar uma aplicação simples "cópia" de um dos módulos da nossa plataforma.

Você deve fazer todos os commits do código fonte diretamente neste repositório ao longo da sua evolução para que possamos acompanhar, ou seja, evite commitar toda a solução de uma única vez.

Dicas:

- Você pode usar ferramentas e bibliotecas open source, mas documente as decisões justificando suas escolhas;
- Automatize o máximo possível;
- Se tiver dúvidas, pergunte.

Pedimos que você faça no arquivo `COMMENTS.md` instruções básicas como:

- como rodar localmente?
- como rodar os testes?
- como fazer o deploy?
- justifique decisões tomadas (utilização de alguma pattern ou biblioteca)

*Pode considerar que eu tenho instalado no meu sistema o Node.js. Qualquer outra dependência que eu precisar você deve fornecer.*

**Prezamos muito por documentação em forma de README**, então faça dele seu cartão de visita.

## Escopo

O objetivo é criar uma aplicação simples que consuma uma API fornecida pela gente que irá te ajudar a fazer uma "cópia" de um dos módulos de nossa plataforma: **Gestão de Usuários**.

A interface implementada deverá ficar o mais próximo possível das telas aqui presentes. Os avaliadores são exigentes!

### Requisitos

- Framework ReactJS (gostamos dele com TypeScript)
- Usar algum pré-processador de CSS (sugerimos SASS)

### Dicas

- Use o Axios para comunicação com a API
- HTML o mais semântico possível
- Use uma boa convenção para os identificadores CSS
- Comprima seus recursos após o build
- Escreva testes
- Para ícones utilize o [Font Awesome](https://fontawesome.com/):
    - [Diretamente do Font Awesome](https://fontawesome.com/start)
    - [React Icons](https://react-icons.netlify.com/#/)
    - [react-fontawesome](https://github.com/FortAwesome/react-fontawesome)
    - Ou outra biblioteca de ícones que implemente o Fontawesome

## Telas

As telas a seguir podem ser inspecionadas através do InVision, clicando no ícone de código no canto inferior direito da tela: https://invis.io/MFTQ3VAGDA8

![Mobile](assets/Mobile.png)

![Desktop](assets/Desktop.png)

### O que você **NÃO** precisa implementar

Os itens a seguir devem estar presentes na tela implementada, mas não precisam ter nenhuma ação:

- Filtros de Status e Papel
- Botão de Ativo/Inativo
- Menu de seleção de Papel (Aluno, Instrutor, etc)
- Ordenação das colunas
- Checkboxes para seleção de múltiplos usuários

### O que você **PRECISA** implementar

- Listagem dos usuários
- Criação de um novo usuário (botão "Adicionar")
- Validação do campo de e-mail ao adicionar
- Botão para excluir usuário 
- Pesquisa de usuários por texto

**Tente implementar o máximo de coisas que conseguir, porém foque em fazer um código simples e limpo**. E lembre, quanto mais coisas implementadas, mais pontos com a gente!

## API

Para este desafio montamos um back-end para simplificar sua implementação! Feito utilizando [json-server](https://github.com/typicode/json-server) a API pode ser encontrada na pasta [backend](backend/).

Caso você já tenha o `json-server` na sua máquina de forma global, basta executar o comando dentro da pasta `backend`:

```
json-server db.json
```

Com essa configuração, seu backend estará escutando em `http://localhost:3000`

Caso **NÃO** tenha o `json-server` em sua máquina, dentro da pasta pasta `backend` execute os comandos:

```
yarn
yarn start
```

Com isso sua aplicação deverá estar rodando no endereço `http://backend.localhost:3333`

Você pode utilizar `npm install` e `npm start` se preferir.

**DICA:** Crie seu front-end de forma que possamos configurar o endpoint utilizado.

### API Endpoints

```
GET /users - Lista usuários
GET /users/:id - Detalhes de um usuário
    - id: Id do usuário
POST /users - Cria novo usuário
    - body (JSON):
        - isActive: Usuário está ativo/inativo (Boolean)
        - avatar: url para avatar do usuário (String)
        - name: nome do usuário (String)
        - email: email do usuário (String)
        - updatedAt: ultima atualização(data de criação/atualização) do usuário (Date)
DELETE /users/:id - Deleta um usuário
    - id: ID do usuário
GET /users?q=:query - Busca usuários por texto
    - query: Termos da pesquisa
```

Caso precise voltar os dados iniciais, copie e cole o contéudo de `db.original.json` em `db.json`.

## O que será avaliado na sua solução?
Queremos ver tudo o que você conhece sobre desenvolvimento front-end. Alguns dos aspectos que serão observados nesse desafio são:

- clareza/objetividade/simplicidade
- estilo do código
- documentação
- testes
- automação
- diferenciais implementados

Boa sorte! ✌