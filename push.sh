#!/bin/bash

# Simply pushes the code into a brand new repository

# Check the number of arguments
if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters. Please use it like this: sh push.sh {repositoryName}"
  exit 1
fi

REPO_NAME=$1

git remote add $REPO_NAME git@bitbucket.org:contextlms/$REPO_NAME.git
git push -u $REPO_NAME master