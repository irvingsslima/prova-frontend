import React, {Fragment, Component} from 'react';
import './App.scss';
import ShowUsers from './components/show_users';
import HeaderUsers from './components/header_users';
import {Container, Section} from 'rbx'
import SearchBox from './components/search_box'
import AddUserHeader from './components/add_user'
import Top from './components/top';


let showAddBar = true;



class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
        showAddBar: false,
        users: [],
      };      
      this.callbackFunction = this.callbackFunction.bind(this)
      this.callbackUsers = this.callbackUsers.bind(this)
    }
    

    callbackFunction = (value) => { 
      this.setState({          
      showAddBar: value
      })
    };

    callbackUsers = (value) => { 
      this.setState({          
      users: value
      })
    };

    
  render() {
  return (
    <Fragment>
    <Section>
      <Container>
      <Top parentCallback = { this.callbackFunction}/>

    <div className="App">
      
        <SearchBox parentCallback = { this.callbackUsers}/>            
        {this.state.showAddBar && <AddUserHeader parentCallback = { this.callbackFunction}/>}        
        <ShowUsers users={ this.state.users}/>
        
    </div>

      </Container>
    </Section>
    </Fragment>
  );
};
}

export default App;
