import axios from "axios";

const url = axios.create({ baseURL:"http://localhost:3001" });

/*
GET /users - Lista usuários
GET /users/:id - Detalhes de um usuário
    - id: Id do usuário
POST /users - Cria novo usuário
    - body (JSON):
        - isActive: Usuário está ativo/inativo (Boolean)
        - avatar: url para avatar do usuário (String)
        - name: nome do usuário (String)
        - email: email do usuário (String)
        - updatedAt: ultima atualização(data de criação/atualização) do usuário (Date)
DELETE /users/:id - Deleta um usuário
    - id: ID do usuário
GET /users?q=:query - Busca usuários por texto
    - query: Termos da pesquisa
    */


export default {
    
  loadUsers: () => url.get(`/users`),

  loadUser: (id) => url.get(`/users/${id}`), 
  
  createUser: (user) => {   
      
    const params = {
    isActive: true,
    avatar: "http://placehold.it/64x64",
    name: user.nome+" "+user.sobrenome,
    email: user.email,
    updatedAt: new Date()
    }

    return url.post(`/users`, params)
},

  searchUsers: (search) => url.get(`users?q=${search}`),  

  deleteUser: (id) => url.delete(`/users/${id}`),

}