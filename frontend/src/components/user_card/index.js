import React, { Component, Fragment } from 'react';
import ListUsers from '../users'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import api from '../../services/api'
import {Box, Button, Checkbox, Media, Image, Content, Level, Icon} from 'rbx'
import { FaReply, FaRetweet, FaHeart } from 'react-icons/fa';

class UserCard extends Component {

    constructor(props) {
        super(props);
        
      }
     

  render() {

    return (
      <Fragment>
        <Media>
          <Media.Item align="left">
            <Image.Container size={40}>
              <Image rounded
                id="avatar"
                alt="Image"
                src={this.props.avatar}
              />
            </Image.Container>
          </Media.Item>
          <Media.Item>
            <Content>
                {this.props.isActive === true ? 
              <p>
                <strong>{this.props.name}</strong> <p><small>{this.props.email}</small></p>
              </p>
              :
              <p className="gray_list">
              <strong>{this.props.name}</strong> <p><small>{this.props.email}</small></p>
            </p>
            }
            </Content>            
          </Media.Item>
        </Media>
        </Fragment>
    );
  }
}


export default UserCard;


