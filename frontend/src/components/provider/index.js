import React, {Fragment, useState, Component} from 'react';

export const SiteThemeContext = React.createContext()
export class SiteThemeProvider extends React.Component {
  state = {
    theme: themes['theme1']
  }
  handleThemeChange = e => {
    const key = e.target.value
    const theme = themes[key]
    this.setState({ theme })
  }


  render() {
    return (
      <SiteThemeContext.Provider
        value={{
          ...this.state,
          handleThemeChange: this.handleThemeChange
        }}>
        {this.props.children}
      </SiteThemeContext.Provider>
    )
  }
}