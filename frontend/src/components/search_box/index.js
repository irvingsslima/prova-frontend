import React, { Component } from 'react';
import { Level, Button, Field, Control, Input, Icon } from 'rbx';
import { FaSearch, FaPlus, FaCheck, FaAngleDown, FaSlidersH } from 'react-icons/fa';
import api from '../../services/api'



class SearchBox extends Component {
  constructor() {
    super();
    this.state = { 
      search: "",
      users: ""
    }
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    this.setState({ search: event.target.value });
  }

  search = (event) => {
    if(event.key === "Enter") {
      this.handleSearch();
    }
  }

  handleSearch = () =>{    
  api.searchUsers(this.state.search).then(response => {
      this.setState({users: response.data })
      console.log("State => "+ JSON.stringify(this.state))
    });

    console.log("Enviando callback/search")
    this.props.parentCallback(this.state.users);
  }
              
  render() {
    return(
      
      <Field className="search-box">
    <Level >
  <Level.Item align="left" >
    <Field id="search-left">
  <Control iconLeft>
    <Input id="search-input" type="text" size="large" placeholder="Procurar usuário..." value={this.state.search} onChange={this.handleChange}
                    onKeyPress={this.search} />
    <Icon align="left" size="medium">
      <FaSearch />
    </Icon>

    </Control>
    
</Field>
  </Level.Item>
  
  <Field id="search-right">
      <span>Status<FaAngleDown/></span>
      &nbsp;
      <span>Papel<FaAngleDown/></span>
      &nbsp;

      <Icon align="right" size="medium">      
      <FaSlidersH/>
    </Icon>
      
    
</Field>
</Level>

        </Field>
    );
  }
}

export default SearchBox;