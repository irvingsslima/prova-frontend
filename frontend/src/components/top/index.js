import React, { Component } from 'react';
import { Level, Button, Title} from 'rbx'
import { FaPlus} from 'react-icons/fa';


class Top extends Component {
    constructor(props) {
        super(props);
        this.state = {
          showAddBar: false
        };
        this.handleAddButton = this.handleAddButton.bind(this);
      }

    formatDate = (fullDate) => {
      let day = new Date(fullDate);
      let date=day.getDate() + "/"+ parseInt(day.getMonth()+1) +"/"+day.getFullYear();
      return date;
      };
     
     handleAddButton = () => {
       this.setState({
         showAddBar: true
       });
      
      this.props.parentCallback(true);

       console.log("ShowAddBar?"+ this.state.showAddBar)
     }


   render() {
     return (
        <div id="top-header">
        <Level >
        <Level.Item align="left" >
        <Title>Usuários</Title>
        </Level.Item>
        <Level.Item align="right">
        <Button color="danger" id="add-button" onClick={() => this.handleAddButton()}><FaPlus/><span>&nbsp; Adicionar</span></Button>  
        </Level.Item>
        </Level>
        </div>
     );
   }
}

export default Top;