import React, { Component } from 'react';
import Card from 'react-bootstrap/Card';
import Table from 'react-bootstrap/Table';
import UserCard from '../user_card';
import { FaTrashAlt, FaAngleDown } from 'react-icons/fa';
import {Box, Button} from 'rbx'
import api from '../../services/api'


class ListUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
          users: []
        };
      }

    formatDate = (fullDate) => {
      let day = new Date(fullDate);
      let date=day.getDate() + "/"+ parseInt(day.getMonth()+1) +"/"+day.getFullYear();
      return date;
      };


   render() {
     return (
       <div>
         
               <Card>
                 <Card.Body>
             <Table >
                   
           <tbody className="users">
             <tr >
              <td className="gray">                
              <input type="checkbox" id="user-checkbox"/>
              </td>

              <td className="gray">ALUNO<FaAngleDown/></td> 
              <td className="gray">SITUAÇÃO</td>
              <td className="gray">PAPEL</td>
              <td className="gray">ATUALIZADO EM </td> 
              <td className="gray"></td> 
              </tr>

               {this.props.users.map((user) => {
                 if(user.isActive === true){
                 return <tr key={user.id} vertical-align="middle">
                <td className="user-checkbox" id="user-checkbox" >
                <input type="checkbox" id="user-checkbox"/>
                </td>                   
                <td className=""><UserCard {...user} /></td> 
                <td className="on"> <Box id="status-box"><div id="green_dot"></div>&nbsp;Ativo</Box> </td>
                <td align="right" className="">Aluno + Instrutor<FaAngleDown /></td>
                <td className="">{this.formatDate(user.updatedAt)}</td>                   
                <td className=""><Button  onClick={ () => api.deleteUser(user.id)} ><FaTrashAlt /></Button></td>
                </tr>;
                 }
                 else{

                  return <tr id= "gray-user" key={user.id}>
                  <td className="user-checkbox" id="user-checkbox" >
                  <input type="checkbox" id="user-checkbox"/>
                  </td>      
                  <td className=""><UserCard {...user} /></td>
                  <td className=""> <Box id="status-box-gray"><div id="red_dot"></div>&nbsp;Suspenso</Box></td>     
                  <td  className="">Aluno<FaAngleDown /></td>
                  <td className="">{this.formatDate(user.updatedAt)}</td>                   
                  <td className=""><Button><FaTrashAlt /></Button></td>
                </tr>;
                 }//else
               })}
             </tbody>
             
           </Table>
             </Card.Body>
               </Card>
           
       </div>
     );
   }
}

export default ListUsers;