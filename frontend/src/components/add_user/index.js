import React, { Component, Fragment } from 'react';
import api from '../../services/api'
import {Button, Input, Field} from 'rbx'


class AddUserHeader extends Component {

    constructor(props) {
        super(props);
        this.state = {
          nome: "",
          sobrenome: "",
          email: ""
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleCancelButton = this.handleCancelButton.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      
       async loadUsers() {
          api.loadUsers().then(response => {
            
          console.log(response);
            this.setState({users: response.data })
          });
          

      }
      
handleInputChange(event) {


  const target = event.target;
  const value = target.value;
  const name = target.name;
  
  this.setState({
    [name]: value
  });
}

resetState(){
  this.setState({
    nome: "",
    sobrenome: "",
    email: ""
  });
}

handleCancelButton(event) {
  this.props.parentCallback(false)
  this.resetState();
}


handleSubmit(event) {
  event.preventDefault();
  api.createUser(this.state).then(response => {
    if(response.status === 201){
      console.log("Dados salvos!")
      this.resetState();
    }
    });
}
    
     

  render() {

    return (
      <Fragment>
      <form onSubmit={this.handleSubmit}>
      <Field id="add-user-header">
    <Input name="nome" value={this.state.value} onChange={this.handleInputChange} id="add-user-input" type="text" size="medium" placeholder="Nome" required/>
  

    <Input name="sobrenome" value={this.state.value} onChange={this.handleInputChange}id="add-user-input" type="text" size="medium" placeholder="Sobrenome" required/>
    <Input name="email" value={this.state.value} onChange={this.handleInputChange} id="add-user-input" type="email" size="medium" placeholder="E-mail" required/>
    <Button onClick={() =>  this.handleCancelButton()  } >Cancelar</Button>   
    <Button color="success"  >Salvar</Button>    
    </Field>
    
    </form>
    </Fragment>
        
    );
  }
}


export default AddUserHeader;


