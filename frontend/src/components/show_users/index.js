import React, { Component, Fragment } from 'react';
import ListUsers from '../users'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import api from '../../services/api'
import AddUserHeader from '../add_user'

class ShowUsers extends Component {

    constructor(props) {
        super(props);
        this.state = {
          users: []
        };
        this.loadUsers = this.loadUsers.bind(this);
      }
      
       async loadUsers() {
          api.loadUsers().then(response => {
            
          console.log(response);
            this.setState({users: response.data })
          });
      }
      
      componentDidMount() {                  
        this.loadUsers();
      }

      componentDidUpdate(prevProps, prevState){
        if(prevState !== this.state)
        this.forceUpdate();

        console.log("State ->" + JSON.stringify(this.state))
        
        console.log("PrevState ->" + JSON.stringify(prevState))

        
        console.log("Props ->" + JSON.stringify(this.props))
        
        console.log("PrevProps ->" + JSON.stringify(prevProps))

        if(prevProps !== this.props)          
          {
            if(this.props.users > 0){
              this.setState({
                users: this.props.users
              })
              this.forceUpdate();
            }else{
              
            console.log("Prev = Props" + JSON.stringify(prevProps))
            console.log("Props ->" + JSON.stringify(this.props))
            //this.loadUsers();
            }
            prevState = this.state;
          }
      }
      

    
     

  render() {

    return (
          <div id="user-list">
            <ListUsers loadUsers={this.loadUsers} users={this.state.users}/>

</div>
    );
  }
}


export default ShowUsers;


